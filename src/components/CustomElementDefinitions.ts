import { ToggleInput } from "./inputs/Toggle"
import { BaseModal } from "./modal/base/BaseModal"
import { BaseModalButton } from "./modal/base/BaseModalButton"
import { BaseModalHeader } from "./modal/base/BaseModalHeader"
import { ModalWrapper } from "./modal/ModalWrapper"
import { OsuModal } from "./modal/osu/OsuModal"
import { OsuModalButton } from "./modal/osu/OsuModalButton"
import { OsuModalHeader } from "./modal/osu/OsuModalHeader"

/**
 * This function registers all components, must be called before using the components
 */
export function initLibraryComponents() {
    customElements.define("input-toggle", ToggleInput, { extends: "div" })
    customElements.define("modal-wrapper", ModalWrapper, { extends: "div" })
    customElements.define("base-modal", BaseModal, { extends: "div" })
    customElements.define("base-modal-button", BaseModalButton, { extends: "button" })
    customElements.define("base-modal-header", BaseModalHeader, { extends: "div" })
    customElements.define("osu-modal", OsuModal, { extends: "div" })
    customElements.define("osu-modal-button", OsuModalButton, { extends: "button" })
    customElements.define("osu-modal-header", OsuModalHeader, { extends: "div" })
}
