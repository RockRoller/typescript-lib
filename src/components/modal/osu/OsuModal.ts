import { osuTheme } from "../../../shared/themes"
import { BaseModal } from "../base/BaseModal"
import { OsuModalHeader } from "./OsuModalHeader"

/**
 * osu! styled modal
 */
export class OsuModal extends BaseModal {
    /**
     * constructor
     * @param modalHeader header of this modal, @see OsuModalHeader
     */
    constructor(modalHeader: OsuModalHeader) {
        super(modalHeader)

        this.closeButton.classList.add("btn-osu-big", `btn-osu-big--forum-secondary`)
        this.closeButton.innerText = "Close"

        this.style.padding = "16px"
        this.style.minWidth = "400px"
        this.style.display = "flex"
        this.style.flexDirection = "column"
        this.style.gap = "8px"
        this.style.borderRadius = "4px"
        this.style.backgroundColor = osuTheme.modalBackground
    }
}
