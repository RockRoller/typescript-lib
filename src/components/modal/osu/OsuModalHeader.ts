import { BaseModalHeader } from "../base/BaseModalHeader"

/**
 * osu! styled modal header
 */
export class OsuModalHeader extends BaseModalHeader {
    /**
     * constructor
     * @param title @see BaseModalHeader
     */
    constructor(title: string) {
        super(title)

        this.style.fontWeight = "700"
        this.style.fontSize = "1.25em"
    }
}
