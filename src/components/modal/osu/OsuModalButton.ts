import { BaseModalButton } from "../base/BaseModalButton"

/**
 * osu! styled modal button
 */
export class OsuModalButton extends BaseModalButton {
    /**
     * constructor
     * @param label @see BaseModalButton
     * @param clickFunction @see BaseModalButton
     * @param buttonType osu! button types @see osuButtonType
     * @param classModifer @see BaseModalButton
     */
    constructor(label: string, clickFunction: () => void, buttonType: osuButtonType = "primary", classModifer?: string) {
        super(label, clickFunction)

        this.classList.add("btn-osu-big", `btn-osu-big--forum-${buttonType}`)
    }
}

/**
 * Different types of buttons, defined by osu-web.
 * primary = green
 * secondary = blue
 */
export type osuButtonType = "primary" | "secondary"
