import { insertStyleTag } from "../../shared/dom"
import { BaseModal } from "./base/BaseModal"

/**
 * Defines the wrapper for all modals.
 * This will auto remove itself (and the containing modal) if a click off the modal happens onto it.
 * .mount() and .unmount() must be called for it to appear/vanish
 */
export class ModalWrapper extends HTMLDivElement {
    /**
     * constructor
     * @param modal modal this wrapper should contain initially
     */
    constructor(private modal: BaseModal) {
        super()

        this.append(this.modal)
        // prevent page background to be scrollable
        insertStyleTag(`.modal-body-freeze { overflow-y: hidden }`)

        this.classList.add("rr-modal-wrapper")
        this.addEventListener("click", () => {
            this.unmount()
        })
        window.addEventListener("keydown", (e) => {
            if (e.key === "Escape") {
                this.unmount()
            }
        })

        this.style.display = "flex"
        this.style.position = "fixed"
        this.style.top = "0"
        this.style.left = "0"
        this.style.width = "100%"
        this.style.height = "100%"
        this.style.zIndex = "1000"
        this.style.backgroundColor = "rgba(0, 0, 0, 0.5)"
        this.style.opacity = "1"
    }

    /**
     * makes this wrapper and its modal visible, has fade-in
     */
    mount() {
        document.body.append(this)
        document.body.classList.add("modal-body-freeze")
    }
    /**
     * makes this wrapper and its modal invisible, has fade-out
     */
    unmount() {
        document.body.classList.remove("modal-body-freeze")
        this.remove()
    }
    /**
     * allows to replace the modal without needing to re-mount a modalwrapper and therefore triggering the animation again
     * @param modal modal to change to, @see BaseModal
     */
    replaceModal(modal: BaseModal) {
        this.modal.replaceWith(modal)
        this.modal = modal
    }
}
