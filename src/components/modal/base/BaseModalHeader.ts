import { IconType } from "../../../constants/types"
import { createElement, createIcon } from "../../../shared/dom"

/**
 * This object describes a base header of a Modal.
 */
export class BaseModalHeader extends HTMLDivElement {
    /**
     * The span displaying the title.
     */
    private headerLabel: HTMLSpanElement = createElement("span", {
        className: "rr-modal__header-label",
    })

    /**
     * The div containing all the icon buttons @see addIconButton
     */
    private iconContainer: HTMLDivElement = createElement("div", {
        className: "rr-modal__header-buttons",
        style: {
            display: "flex",
            alignItems: "center",
            gap: "8px",
        },
    })

    /**
     * constructor
     * @param title title that the header should display
     */
    constructor(title: string) {
        super()

        this.classList.add("rr-modal__header")

        this.headerLabel.innerText = title
        this.style.display = "flex"
        this.style.justifyContent = "space-between"
        this.style.alignItems = "center"

        this.append(this.headerLabel)
        this.append(this.iconContainer)
    }

    /**
     * This method adds an iconButton to the header.
     * More than one icon should be possible.
     * @param iconName icon to be used
     * @param tooltip tooltip for the button
     * @param clickFunction function for "click" EventListener
     */
    public addIconButton(iconName: IconType, tooltip: string, clickFunction: () => void) {
        this.iconContainer.append(
            createElement("div", {
                style: {
                    display: "flex",
                    cursor: "pointer",
                },
                attributes: {
                    title: tooltip,
                    onclick: clickFunction,
                },
                children: [createIcon(iconName)],
            })
        )
    }

    /**
     * This method updates the title of the header.
     * @param headerTitle title to be displayed
     */
    public setTitle(headerTitle: string) {
        this.headerLabel.innerText = headerTitle
    }
}
