/**
 * This object defines a base modal button with "click" EventListener.
 */
export class BaseModalButton extends HTMLButtonElement {
    /**
     * constructor
     * @param label label of the button
     * @param clickFunction "click" event function
     * @param classModifier optional class modifier for this element
     */
    constructor(label: string, clickFunction: () => void, classModifier?: string) {
        super()

        this.classList.add("rr-modal__button")
        if (classModifier) {
            this.classList.add(`rr-modal__button--${classModifier}`)
        }
        this.innerText = label
        this.addEventListener("click", clickFunction)
    }
}
