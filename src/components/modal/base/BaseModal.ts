import { IconType } from "../../../constants/types"
import { createElement } from "../../../shared/dom"
import { ModalWrapper } from "../ModalWrapper"
import { BaseModalButton } from "./BaseModalButton"
import { BaseModalHeader } from "./BaseModalHeader"

export class BaseModal extends HTMLDivElement {
    /**
     * Container containing all the iconButtons
     */
    protected buttonContainer: HTMLDivElement = createElement("div", {
        className: "rr-modal__buttons-container",
        style: {
            display: "flex",
            gap: "8px",
        },
    })
    /**
     * Container containing the actual modal content
     */
    private contentBox: HTMLDivElement = createElement("div", {
        style: {
            gap: "inherit",
            display: "flex",
            flexDirection: "column",
            margin: "0 8px",
            maxHeight: "60vh",
            overflowY: "auto",
        },
        className: "rr-modal__content-box",
    })
    /**
     * Default close button
     */
    protected closeButton: BaseModalButton = new BaseModalButton("close", () => {
        ;(this.parentElement as ModalWrapper).unmount()
    })

    /**
     * constructor
     * @param header @see BaseModalHeader
     */
    constructor(private header: BaseModalHeader) {
        super()
        this.classList.add("rr-modal")
        this.addEventListener("click", (e) => {
            e.preventDefault()
            e.stopPropagation()
            e.stopImmediatePropagation()
        })

        this.style.margin = "auto"

        this.buttonContainer.append(this.closeButton)
        this.append(this.header)
        this.append(this.contentBox)
        this.append(this.buttonContainer)
    }

    /**
     * This method adds content to the bottom of the modals content box
     * @param content any HTMLElement
     */
    public addContent(content: HTMLElement) {
        this.contentBox.append(content)
        content.scrollIntoView()
    }
    /**
     * This method adds a new button to the modal. New buttons appear to the left of existing ones by default.
     * @see ModalButton
     * @param button @see BaseModalButton
     * @param after defines if the elements should appear to the right of existing buttons, false by default
     */
    public addModalButton(button: BaseModalButton, after: boolean = false) {
        if (after) {
            this.buttonContainer.append(button)
        } else {
            this.buttonContainer.prepend(button)
        }
    }
    /**
     * adds an iconButton to the header, for details please see @see BaseModalHeader
     * @param iconName @see BaseModalHeader.addIconButton
     * @param tooltip @see BaseModalHeader.addIconButton
     * @param eventFunction @see BaseModalHeader.addIconButton
     */
    public addIconButton(iconName: IconType, tooltip: string, eventFunction: () => void) {
        this.header.addIconButton(iconName, tooltip, eventFunction)
    }
    /**
     * This method updates the title of the header.
     * @param title title to be displayed
     */
    public setTitle(title: string) {
        this.header.setTitle(title)
    }
}
