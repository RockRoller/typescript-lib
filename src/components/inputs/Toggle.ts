import { createElement } from "../../shared/dom"

/**
 * defines a toggle input element
 */
export class ToggleInput extends HTMLDivElement {
    /**
     * constructor
     * @param clickFunction the function to be executed when this toggle is clicked
     */
    constructor(clickFunction: () => void) {
        super()

        this.classList.add("rr-toggle")
        this.addEventListener("click", clickFunction)
        this.append(
            createElement("div", {
                className: "rr-toggle__background",
            })
        )
        this.append(
            createElement("div", {
                className: "rr-toggle__knob",
            })
        )
    }
    /**
     * set state inactive
     */
    setActive() {
        this.classList.add("active")
    }
    /**
     * set state inactive
     */
    setInActive() {
        this.classList.remove("active")
    }
    /**
     * toggle active state
     */
    toggleState() {
        this.classList.contains("active") ? this.classList.remove("active") : this.classList.add("active")
    }
}
