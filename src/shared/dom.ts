import * as icons from "../constants/icons"
import { CreateElementOptions, CreateIconOptions, IconType } from "../constants/types"
import { ensuredSelector, wait } from "./common"
import { MissingElementException } from "./exceptions"

/**
 * creates an element from string by using innerHTML
 * @param html any valid html as string
 * @returns element
 */
export function createElementFromHTMLString<T extends Element>(html: string): T {
    return createElement("div", {
        attributes: {
            innerHTML: html,
        },
    }).firstChild as T
}

/**
 * easy creation of html elements with styles and attributes
 * @param tagName HTML tag name of the element, e.g. "div"
 * @param options @see CreateElementOptions
 * @returns element
 */
export function createElement<T extends keyof HTMLElementTagNameMap>(tagName: T, options: CreateElementOptions<T> = {}) {
    const { attributes = {}, style = {}, className = "", children = [], events = [] } = options
    const element = document.createElement(tagName)

    Object.assign(element, { ...attributes, className: `rr-lib ${className}` })
    Object.assign(element.style, style)
    element.append(...children)
    events.forEach((event) => element.addEventListener(event.type, event.handler))

    return element
}

/**
 * waits until the given element is found
 * @param query element to wait for
 * @param parent element/document to wait within
 * @returns the element
 */
export async function waitForElement<T extends Element>(query: string, parent: HTMLElement | Document = document) {
    for (let tries = 0; tries < 200; tries++) {
        const element = parent.querySelector<T>(query)
        if (element) {
            return element
        }
        await wait(100)
    }
    throw new MissingElementException(`waitForElement timed out for: ${query}`)
}

/**
 * inserts the given style rule into the document
 * @param styleText text of one or more css rules
 */
export function insertStyleTag(styleText: string) {
    ensuredSelector("head").append(
        createElement("style", {
            attributes: {
                innerHTML: styleText,
            },
        })
    )
}

/**
 * creates an icon, all icons are in icons.ts
 * @param name key of the icon in icons.ts
 * @param options @see CreateIconOptions
 * @returns icon element
 */
export function createIcon(name: IconType, options: CreateIconOptions = {}) {
    const { size = 16, ...rest } = options
    const element = createElementFromHTMLString<SVGElement>(icons[name])

    Object.assign(element.style, {
        width: `${size}px`,
        height: `${size}px`,
        ...rest,
    })

    return element
}
