export const osuTheme = {
    successColor: "#b3d944",
    failureColor: "#d94444",
    normalTextColor: "hsl(var(--hsl-c1))",
    notificationBackground: "hsl(var(--hsl-b4))",
    secondaryBackground: "hsl(var(--hsl-b4))",
    modalBackground: "hsl(var(--hsl-b2))",
    inputMinWidth: "250px",
    osuPink: "hsl(var(--hsl-h1))",
}