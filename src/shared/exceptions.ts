/**
 * Exception thrown when ensuredSelectorand waitForElement can not assure the existance of the input
 */
export class MissingElementException extends Error {
    constructor(message: string) {
        super(message)
        this.name = "MissingElementException"
    }
}

/**
 * Exception thrown when GM_getValue returns undefined
 */
export class UndefinedStorageValueException extends Error {
    constructor(message: string) {
        super(message)
        this.name = "UndefinedStorageValueException"
    }
}

