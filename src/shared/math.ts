/**
 * This function returns a random integer between 0 and max-1.
 * @param max Maximum integer
 * @returns Integer between 0 and max -1
 */
export function getRandomInt(max: number) {
    return Math.floor(Math.random() * Math.floor(max))
}

/**
 * This function returns a random integer between 1 and sides, just like a dice would
 * @param sides number of dice sides
 * @returns
 */
export function getDiceRoll(sides: number) {
    return getRandomInt(sides) + 1
}
