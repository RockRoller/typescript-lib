import { createElement } from "./dom"
import { MissingElementException } from "./exceptions"

/**
 * waits for the supplied amount of time
 * @param ms time in miliseconds
 * @returns
 */
export function wait(ms: number) {
    return new Promise((r) => setTimeout(r, ms))
}

/**
 * ensures that an element exists in the document and returns this element
 * @param query any valid css selector, @see document.querySelector
 * @throws MissingElementError when element can not be found in document, aka is null or undefined
 * @returns selected element
 */
export function ensuredSelector<T extends Element>(query: string, parent: Document | HTMLElement = document): T {
    const element = parent.querySelector<T>(query)
    if (!element) {
        throw new MissingElementException(`No element found for query: "${query}"`)
    }
    return element
}

/**
 * downloads a file by using HTML5 anchor download attribute
 * @param url location of the file
 * @param filename name of the file, defaults to timestamp
 */
export async function downloadUrl(url: string, filename: string = Date.now().toString()) {
    const blobUrl = URL.createObjectURL(await (await fetch(url)).blob())
    const anchor = createElement("a", {
        attributes: {
            href: blobUrl,
            download: filename,
        },
    })
    document.body.append(anchor)
    anchor.click()
    anchor.remove()
    URL.revokeObjectURL(blobUrl)
}
