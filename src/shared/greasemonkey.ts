import { UndefinedStorageValueException } from "./exceptions"

declare function GM_setValue(key: string, value: string | boolean | number): void
declare function GM_getValue(key: string): string | boolean | number
declare function GM_listValues(): Array<string>
declare function GM_deleteValue(key: string): void

/**
 * set the specified variable in greasemonkey storage
 * @param key storage key
 * @param value value
 */
export function gmSetValue(key: string, value: string | boolean | number) {
    GM_setValue(key, value)
}

/**
 * get the specified variable from greasemonkey storage
 * @param key storage key
 * @returns value from storage
 * @throws UndefinedStorageValueException if GM/TM fails and finds an undefined value
 */
export function gmGetValue(key: string) {
    const value = GM_getValue(key)
    if (value == undefined) {
        throw new UndefinedStorageValueException("Undefined value for key: " + key)
    } else {
        return value
    }
}

/**
 * get all values from greasemonkey storage
 * @returns
 */
export function gmListValues() {
    return GM_listValues()
}

/**
 * delete specified variable from greasemonkey storage
 * @param key storage key
 */
export function gmDeleteValue(key: string) {
    GM_deleteValue(key)
}
