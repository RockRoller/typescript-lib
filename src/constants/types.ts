import * as icons from "./icons"

/**
 * options for createElement
 */
export type CreateElementOptions<T extends keyof HTMLElementTagNameMap> = {
    /**
     * class attribute
     */
    className?: string
    /**
     * style attribute
     */
    style?: Partial<CSSStyleDeclaration>
    /**
     * child elements
     */
    children?: (Element | string)[]
    /**
     * all other attributes applicable to HTMLElementTagNameMap[T]
     */
    attributes?: Partial<HTMLElementTagNameMap[T]>

    /**
     * eventlisteners for this element
     */
    events?: EventHandler[]
}

/**
 * data required to add an eventListener, @see addEventListener in TS definitions
 */
type EventHandler = {
    type: string
    handler: EventListenerOrEventListenerObject
    options?: boolean | AddEventListenerOptions
}

/**
 * transforms icons.ts into a type
 */
export type IconType = keyof typeof icons

/**
 * options for createIcon, adds size parameter
 * removes css width and height as these are bound to size
 */
export type CreateIconOptions = Partial<Omit<CSSStyleDeclaration, "width" | "height">> & {
    /**
     * size that should be used for width and height (px)
     */
    size?: number
}
